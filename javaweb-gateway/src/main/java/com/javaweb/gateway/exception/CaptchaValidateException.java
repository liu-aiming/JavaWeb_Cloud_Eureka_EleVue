package com.javaweb.gateway.exception;

/**
 * <p>
 * 验证码异常处理类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-07
 */
public class CaptchaValidateException extends RuntimeException {

    private static final long serialVersionUID = -7285211528095468156L;

    public CaptchaValidateException() {
    }

    public CaptchaValidateException(String msg) {
        super(msg);
    }

}
